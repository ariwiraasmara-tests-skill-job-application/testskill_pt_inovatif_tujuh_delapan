class Movie {
  final int id;
  final String lang;
  final String title;
  final String overview;
  final String image;
  final String popularity;
  final String release_date;
  final int vote_average;
  final int vote_count;

  Movie(this.id, this.lang, this.title, this.overview, this.image, this.popularity, this.release_date, this.vote_average, this.vote_count); 
  factory Movie.fromMap(Map<String, dynamic> json) { 
      return Movie( 
        json['id'], 
        json['lang'], 
        json['title'], 
        json['overview'], 
        json['backdrop_path'], 
        json['popularity'], 
        json['release_date'], 
        json['vote_average'], 
        json['vote_count'], 
      );
   }
}